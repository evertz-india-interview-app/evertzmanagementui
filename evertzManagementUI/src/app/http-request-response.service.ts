import { Injectable } from '@angular/core';
import { SystemConfig } from './wsCall/system-config';
import { Login } from "./wsCall/login";
import {  HttpClient, HttpHeaders, HttpErrorResponse,HttpResponse} from "@angular/common/http";
import { Observable } from 'rxjs';
import { validateServer } from './wsCall/validateServer';
import { uiDropdown } from './wsCall/uiDropdown';
import { addEmployee } from './wsCall/addEmployee';
import { Logout } from './wsCall/logout';
import { getFunction } from './wsCall/functionGet';
import { GetProfilePic } from './wsCall/getProfilePic';
import { UpdateProfilePic } from './wsCall/updateProfilePic';
import { GetBankBranch } from './wsCall/uiGetBankBranch';
import { updateFirstTimeDetails } from './wsCall/updateFirstTimeDeails';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestResponseService {

  constructor(private systemConfig: SystemConfig, private httpClientObject: HttpClient) { }

  public serverIpAddress = this.systemConfig.serverIp;
  private urlWithEndPoint = "http://" + this.serverIpAddress + ":8080/api";

  login(userName: Login,password: Login): Observable<HttpResponse<Login>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Login",
          Command: "Login",
          ParameterList: {
                  "UserName": userName,
                  "Password": password
              }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<Login>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    logout(employeeId: Logout): Observable<HttpResponse<Logout>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Login",
          Command: "Logout",
          ParameterList: {
                "EmployeeCode": String(employeeId)
              }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<Logout>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    validateServer(serverIp: validateServer): Observable<HttpResponse<validateServer>> {
      console.log();
      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Server",
          Command: "ValidateServer",
          ParameterList: {
                  "ServerIp": serverIp
              }
          }
      }
  
      return this.httpClientObject.post<validateServer>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });
      
    }

    getUIDropdown(options: uiDropdown,employeeCode: uiDropdown): Observable<HttpResponse<uiDropdown>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "UI",
          Command: "Get",
          ParameterList: {
                  "EmployeeCode": employeeCode,
                  "UIDropdownField": options
              }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<uiDropdown>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    getBankBranchList(bankId: GetBankBranch,employeeCode: GetBankBranch): Observable<HttpResponse<GetBankBranch>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "UI",
          Command: "GetBankBranch",
          ParameterList: {
            "BankNameId": bankId,
            "EmployeeCode": employeeCode
            }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<GetBankBranch>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    getFunctionList(useraccess: getFunction): Observable<HttpResponse<getFunction>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Function",
          Command: "Get",
          ParameterList: {
                "UserAccess": useraccess
              }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<getFunction>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    addEmployee(firstName: addEmployee, middleName: addEmployee, lastName: addEmployee, gender: addEmployee,
      doj: addEmployee, designation: addEmployee, team: addEmployee, region: addEmployee,department: addEmployee, photoPath: addEmployee,
      useraccess: addEmployee,password: addEmployee): Observable<HttpResponse<addEmployee>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Employee",
          Command: "AddEmployee",
          ParameterList: {
                "FirstName": firstName,
                "MiddleName": middleName,
                "LastName": lastName,
                "GenderId": gender,
                "DateOfJoin": doj,
                "DesignationId": designation,
                "TeamId": team,
                "RegionId": region,
                "DepartmentId": department,
                "DefaultPhotoPath": photoPath,
                "UserAccess": useraccess,
                "Password": password
              }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<addEmployee>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    getProfilePic(employeeCode: GetProfilePic): Observable<HttpResponse<GetProfilePic>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Profile",
          Command: "GetProfilePic",
          ParameterList: {
                "EmployeeCode": employeeCode
              }
          }
      }
      console.log(parameters);
  
      return this.httpClientObject.post<GetProfilePic>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    updateProfilePic(employeeCode: UpdateProfilePic,profilePicBase64: UpdateProfilePic): Observable<HttpResponse<UpdateProfilePic>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Profile",
          Command: "UpdateProfilePic",
          ParameterList: {
                "EmployeeCode": employeeCode,
                "ProfilePicBase64": profilePicBase64
              }
          }
      }
      console.log(parameters);
  
      return this.httpClientObject.post<UpdateProfilePic>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }

    updateEmployeeFirstTimeDetails(employeeCode: updateFirstTimeDetails,employeeId: updateFirstTimeDetails,dob: updateFirstTimeDetails,
      emailId: updateFirstTimeDetails,mobileNumber: updateFirstTimeDetails,isGrde: updateFirstTimeDetails,
      eduType: updateFirstTimeDetails,eduBranch: updateFirstTimeDetails,scoreValue: updateFirstTimeDetails,
      doc1Id: updateFirstTimeDetails,doc1Val: updateFirstTimeDetails,doc2Id: updateFirstTimeDetails,doc2Val: updateFirstTimeDetails,
      isExperienced: updateFirstTimeDetails,companyName: updateFirstTimeDetails,designation: updateFirstTimeDetails,workedFrom: updateFirstTimeDetails,
      workedTill: updateFirstTimeDetails,isVisaHolder: updateFirstTimeDetails,isPassportHolder: updateFirstTimeDetails,nameOnPassport: updateFirstTimeDetails,
      passportNumber: updateFirstTimeDetails,passportIssueDate: updateFirstTimeDetails,passportExpDate: updateFirstTimeDetails,visaCountry: updateFirstTimeDetails,
      visaNumber: updateFirstTimeDetails,visaExpDate: updateFirstTimeDetails,bankAccNumber: updateFirstTimeDetails,bankAccHolderName: updateFirstTimeDetails,
      bankBranchId: updateFirstTimeDetails): Observable<HttpResponse<updateFirstTimeDetails>>{

      var parameters = {
        EvertzManagementApp: {
          Subsystem: "Employee",
          Command: "UpdateFirstTimeDetails",
            ParameterList: {
              "EmployeeCode": employeeCode,
              "EmployeeId": employeeId,
              "DateOfBirth": dob,
              "EmailId": emailId,
              "MobileNumber": mobileNumber,
              "IsGrade": isGrde,
              "EducationDetails": {
                "EducationType": eduType,
                "Educationbranch": eduBranch,
                "ScoreValue": scoreValue
              },
              "DocumentList": {
                "Document1Id": doc1Id,
                "Document1Value": doc1Val,
                "Document2Id": doc2Id,
                "Document2Value": doc2Val
              },
              "IsExperienced": isExperienced,
              "ExperienceDetails": {
                "CompanyName": companyName,
                "Designation": designation,
                "WorkedFrom": workedFrom,
                "WorkedTill": workedTill
              },
              "IsVisaHolder": isVisaHolder,
              "IsPassportHolder": isPassportHolder,
              "PassportDetails": {
                "NameOnPassport": nameOnPassport,
                "PassportNumber": passportNumber,
                "DateOfPassportIssue": passportIssueDate,
                "DateOfPassportExpirey": passportExpDate
              },
              "VisaDetails": {
                "VisaCountry": visaCountry,
                "VisaNumber": visaNumber,
                "DateOfVisaExpirey": visaExpDate
              },
              "BankDetails": {
                "AccNumber": bankAccNumber,
                "AccHolderName": bankAccHolderName,
                "BankBranch": bankBranchId
              }
            }
          }
      }
      console.log(parameters);

      return this.httpClientObject.post<updateFirstTimeDetails>(`${this.urlWithEndPoint}`, parameters,{
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authentication": "frontendui|frontendui"
       }),
        observe: 'response'
      });    
  
    }
  
}
