export class SystemConfig {

    constructor() {}

    //general settings
    public serverIp = "localhost";
    public errorResetTime = 20; // in seconds
    public sessionTime = 300; //in seconds
    public rightClickDirective = "appNoRightClick";
    public profileLoadingPic = "assets/defaults/evertz-logo.png";

    //access settings
    public employeeAccessCode = "1";
    public adminAccessCode = "5";

    //path settings
    public redirectURL = "redirect";
    public employeeLogin = "login";
    public employeehome = "ehome";
    public adminLogin = "admin.login";
    public adminHome = "admin.home";
    public adminTools = "admin.tools";
    public newEmployeeDetails = "edetails";

    //defaults
    public defaultNewEmployeePassword = "evertz@123";
    public defaultProfileMale = "assets/defaults/profileMale.png";
    public defaultProfileFeale = "assets/defaults/profileFemale.png";
    public defaultProfileOthers = "assets/defaults/profileOthers.png";

    //Bootstrap settings
    public errorClass = "alert alert-danger alert-dismissible";
    public successClass= "alert alert-success alert-dismissible";

}