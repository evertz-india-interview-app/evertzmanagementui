import { TestBed } from '@angular/core/testing';

import { HttpRequestResponseService } from './http-request-response.service';

describe('HttpRequestResponseService', () => {
  let service: HttpRequestResponseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpRequestResponseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
