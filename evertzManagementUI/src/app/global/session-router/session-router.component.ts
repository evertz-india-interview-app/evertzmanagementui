import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { AppFunctions } from 'src/app/app-functions';

@Component({
  selector: 'app-session-router',
  templateUrl: './session-router.component.html',
  styleUrls: ['./session-router.component.css']
})
export class SessionRouterComponent implements OnInit {

  public redirectTo = "";
  constructor(private router: ActivatedRoute,private systemConfig: SystemConfig,private redirect: Router,private golbalFuncs: AppFunctions) { 

    this.router.queryParams.subscribe(params => {
      this.redirectTo = params["redirectTo"];
         
  });

  }

  ngOnInit(): void {
    console.log(this.redirectTo);

    if(this.redirectTo == "1"){
      //this.systemConfig.resetSession();
      this.redirect.navigate([this.systemConfig.employeehome]);
    }else if(this.redirectTo == "2"){
      this.golbalFuncs.resetSession();;
    }else if(this.redirectTo == "3"){
      this.golbalFuncs.resetSession();
    }else if(this.redirectTo == "4"){
      this.golbalFuncs.resetSession();
    }else if(this.redirectTo == "5"){
      //this.systemConfig.resetSession();
      this.redirect.navigate([this.systemConfig.adminHome]);
    }else{
      this.golbalFuncs.resetSession();
      console.log(this.redirectTo);
      console.log("invalid code received, redirect to employee login");
      this.redirect.navigate([this.systemConfig.employeehome]);
    }    

  }
  
  
}
