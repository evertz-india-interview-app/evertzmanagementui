import { Component, OnInit, HostListener } from '@angular/core';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SessionDialogueComponent } from 'src/app/global/session-dialogue/session-dialogue.component';
import { AppFunctions } from 'src/app/app-functions';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  
  public rightClickDirective: any;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  public employeeName: any;
  public employeeCode: any;
  public employeeProfilePic: any;
  public isSessionTimedOut = false;
  public sessionResetTime = this.systemConfig.sessionTime;


  constructor(private dialog: MatDialog,private systemConfig: SystemConfig,
    private postToServerObject: HttpRequestResponseService,private redirect: Router,private golbalFuncs: AppFunctions) { }

  ngOnInit(): void {

    if(this.golbalFuncs.isSessionSet() && parseInt(localStorage.getItem("UserAccess"))==5){

      this.golbalFuncs.isSessionTimedOut = false;
      this.golbalFuncs.startSessionTimer();
      this.employeeCode = localStorage.getItem("EmployeeCode");
      this.employeeName = localStorage.getItem("EmployeeName");
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
      if(this.employeeProfilePic == null){
        this.employeeProfilePic = this.systemConfig.profileLoadingPic;
      }

    }else{
      //invalid session
      this.golbalFuncs.resetSession();
      this.redirect.navigate([this.systemConfig.adminLogin]);
    }

    if(this.golbalFuncs.isNewUser()){
      this.redirect.navigate([this.systemConfig.newEmployeeDetails]);
    }

  }

  currDiv: string = 'none';
  @HostListener('click') onSelect(divVal:string){
    if(this.currDiv=='none'){
      this.currDiv = divVal;
    }
    else{
      this.currDiv='none';
    }
  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  @HostListener('click') onClick() {
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
    
  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {

    if(this.employeeProfilePic != localStorage.getItem(this.golbalFuncs.userProfilePic)){
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
    }
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }


  checkDropDown(){

    if(this.currDiv=='dropdown-content'){
      this.currDiv='none';
    }
  }


  gotoHome(){
    this.redirect.navigate([this.systemConfig.adminHome]);
   }

  gotoTools(){
  this.redirect.navigate([this.systemConfig.adminTools]);
  }
  
  userLogout(){
    this.golbalFuncs.userLogout();
  }
}
