import { Component, OnInit, HostListener } from '@angular/core';
import { SystemConfig } from 'src/app/wsCall/system-config';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';
import { SessionDialogueComponent } from 'src/app/global/session-dialogue/session-dialogue.component';
import { Router } from '@angular/router';
import { LoadingBarService } from '../../../../node_modules/ngx-loading-bar';
import { AppFunctions } from 'src/app/app-functions';


@Component({
  selector: 'app-admin-tools',
  templateUrl: './admin-tools.component.html',
  styleUrls: ['./admin-tools.component.css']
})


export class AdminToolsComponent implements OnInit {

  
  public employeeName: any;
  public employeeCode: any;
  public toolsOptionsList = [];
  public toolsCssHighlighter = [];
  public normalBtnCss = "sidenav-sub-btn";
  public highlightedBtnCss = "sidenav-sub-btn-change";
  public displayFunction: any;
  public displayFunctionCode = "";
  public showToolsOptions = "true";
  public displayLoading = "false";
  public designation:any;
  public gender:any;
  public teams:any;
  public region:any;
  public department:any;
  public userAccess:any;
  public employeeProfilePic: any;
  public showProgressBar = false;
  public isSessionTimedOut = false;
  public serverResponse = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  public sessionResetTime = this.systemConfig.sessionTime;
  sessionseconds: number = this.systemConfig.sessionTime;
  resetseconds: number = this.systemConfig.errorResetTime;
  interval;
  currDiv: string = 'none';


  constructor(private dialog: MatDialog,private postToServerObject: HttpRequestResponseService,private systemConfig: SystemConfig,
    private redirect: Router,private loadingBar: LoadingBarService,private golbalFuncs: AppFunctions) { }

  ngOnInit(): void {

    if(this.golbalFuncs.isSessionSet() && parseInt(localStorage.getItem("UserAccess"))==5){

      this.golbalFuncs.isSessionTimedOut = false;
      this.golbalFuncs.startSessionTimer();
      this.employeeCode = localStorage.getItem("EmployeeCode");
      this.employeeName = localStorage.getItem("EmployeeName");
      this.userAccess = localStorage.getItem("UserAccess");
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
      if(this.employeeProfilePic == null){
        this.employeeProfilePic = this.systemConfig.profileLoadingPic;
      }

      //To avoid error setting default values
      this.toolsOptionsList = [{
        "SortOrder": 0,
        "FunctionName": "Loading Functions...",
        "FunctionCode": 0
      }];
      this.toolsCssHighlighter[0] = this.normalBtnCss;
      this.displayFunction = this.toolsOptionsList[0];
      this.displayFunctionCode = "0";

      //calling backend to get necessary funcitons
      this.getFunctions(String(this.userAccess));

    }else{

      //invalid session
      this.golbalFuncs.resetSession();
      this.redirect.navigate([this.systemConfig.adminLogin]);
    }

    if(this.golbalFuncs.isNewUser()){
      this.redirect.navigate([this.systemConfig.newEmployeeDetails]);
    }
    
  }


  @HostListener('click') onSelect(divVal:string){
    if(this.currDiv=='none'){
      this.currDiv = divVal;
    }
    else{
      this.currDiv='none';
    }
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.resetseconds > 0) {
        this.resetseconds--;
      } else {
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
    this.resetseconds = this.systemConfig.errorResetTime;
  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  @HostListener('click') onClick() {
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {

    if(this.employeeProfilePic != localStorage.getItem(this.golbalFuncs.userProfilePic)){
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
    }
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  public closealertbtn(){
    this.serverStatus_Error_Class = "";
    this.serverStatus_Error_Message = "";
  }

  startLoading() {
    this.loadingBar.start();
  }

  stopLoading() {
    this.loadingBar.complete();
  }

  loadingComplete(){
    this.loadingBar.complete();
  }
  checkDropDown(){

    if(this.currDiv=='dropdown-content'){
      this.currDiv='none';
    }
  }

  showTools(){
    if(this.showToolsOptions == "true"){
      this.showToolsOptions= "false";
    }else{
      this.showToolsOptions = "true";
    }
  }


gotoTop() {
  window.scroll({ 
    top: 0, 
    left: 0, 
    behavior: 'smooth' 
  });
}
 gotoHome(){
  this.redirect.navigate([this.systemConfig.adminHome]);
 }

  userLogout(){
    this.golbalFuncs.userLogout();
  }

  changeHighlighterMoveToFunction(moveToFunctionId){

    for (let i=0; i<this.toolsCssHighlighter.length; i++){
        if(this.toolsCssHighlighter[i] == "sidenav-sub-btn-change"){
          this.toolsCssHighlighter[i] = "sidenav-sub-btn";
          break;
        }
    }

    this.toolsCssHighlighter[parseInt(moveToFunctionId)] = "sidenav-sub-btn-change";
    this.displayFunctionCode = this.toolsOptionsList[parseInt(moveToFunctionId)].FunctionCode;
    this.displayFunction = this.toolsOptionsList[parseInt(moveToFunctionId)];
    
    this.functionMapper();

  }

  functionMapper(){

    if(parseInt(this.displayFunctionCode) == 1){
      this.displayLoading = "true";
      this.addEmployeeUIDropDown();

    }

  }

  addEmployeeUIDropDown(){
    
    var options: any;
    var tmp = this.displayFunctionCode;
    this.displayFunctionCode = "0";
    options= ["Gender","Team","Designation","Region","Department","UserAccess"];
    this.postToServerObject.getUIDropdown(options,this.employeeCode).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: Boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      this.displayLoading = "false";
      this.displayFunctionCode = tmp;
      if(!serverStatus){
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        console.log(this.serverStatus_Error_Message);
        this.startTimer();
      }else{
        this.gender = this.serverResponse["EvertzManagementApp"]["ParameterList"]["GenderList"];
        this.teams = this.serverResponse["EvertzManagementApp"]["ParameterList"]["TeamList"];
        this.department = this.serverResponse["EvertzManagementApp"]["ParameterList"]["DepartmentList"];
        this.region = this.serverResponse["EvertzManagementApp"]["ParameterList"]["RegionList"];
        this.designation = this.serverResponse["EvertzManagementApp"]["ParameterList"]["DesignationList"];
        this.userAccess = this.serverResponse["EvertzManagementApp"]["ParameterList"]["UserAccesstList"];
      }

    },(err: HttpErrorResponse) => { 
        this.displayLoading = "false"; 
        this.displayFunctionCode = tmp;
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = "Failed to Connect to Server, please try again latter.";
        this.startTimer();
    }); 
  }



  addEmployeeToDB(firstname,middlename,lastname,gender,doj,designation,team,worklocation,dept,useraccess){

    this.showProgressBar = true;
    
    if(firstname == "" || lastname=="" || gender == "" || doj == "" || designation == "" ||
    team == "" || worklocation == "" || dept == "" || useraccess == ""){
      this.showProgressBar = false;
    }else{


      let profilePath:any;
      let pwd:any
    
      if(gender == 1){
        profilePath = this.systemConfig.defaultProfileMale;
      }else if(gender == 2){
        profilePath = this.systemConfig.defaultProfileFeale;
      }else if(gender == 3){
        profilePath = this.systemConfig.defaultProfileOthers;
      }


      doj = this.golbalFuncs.formatDate(doj);
      pwd = this.systemConfig.defaultNewEmployeePassword;
      this.postToServerObject.addEmployee(firstname,middlename,lastname,gender,doj,
        designation,team,worklocation,dept,profilePath,useraccess,pwd).subscribe((res: HttpResponse<any>)=> {
        var serverStatus: boolean;
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
        console.log(this.serverResponse);
        if(!serverStatus){
          this.showProgressBar = false;
          this.serverStatus_Error_Class = this.systemConfig.errorClass;
          this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
          console.log(this.serverStatus_Error_Message);
          this.gotoTop();
          this.startTimer();
        }else{
          this.serverStatus_Error_Class = this.systemConfig.successClass;
          this.serverStatus_Error_Message = "Employee Addedd Sucessfully, Employee Code is " + this.serverResponse["EvertzManagementApp"]["ParameterList"]["EmployeeCode"];
          console.log(this.serverStatus_Error_Message);
          this.showProgressBar = false;
          this.gotoTop();
          this.startTimer();
        }
  
      },(err: HttpErrorResponse) => {
        this.showProgressBar = false;  
        this.serverStatus_Error_Class = this.systemConfig.errorClass;
          this.serverStatus_Error_Message = "Connection timed out, please try again latter.";
          this.gotoTop();
          this.startTimer();
      }); 

    }

  }

  getFunctions(userAccess){

    this.postToServerObject.getFunctionList(userAccess).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(!serverStatus){
        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        this.startTimer();
      }else{
        this.toolsOptionsList = this.serverResponse["EvertzManagementApp"]["ParameterList"]["FunctionList"];
        this.toolsCssHighlighter[0] = this.highlightedBtnCss;
        for(let i=1; i<this.toolsOptionsList.length; i++){
          this.toolsCssHighlighter[i] = this.normalBtnCss;
        }

        this.displayFunctionCode = this.toolsOptionsList[0].FunctionCode;
        this.displayFunction = this.toolsOptionsList[0];
        this.functionMapper(); 
      }

    },(err: HttpErrorResponse) => {  
      this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = "Connection timed out, please try again latter.";
        this.startTimer();
    });    
  }

}
