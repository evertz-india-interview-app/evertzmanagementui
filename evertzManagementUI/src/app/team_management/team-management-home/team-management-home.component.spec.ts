import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamManagementHomeComponent } from './team-management-home.component';

describe('TeamManagementHomeComponent', () => {
  let component: TeamManagementHomeComponent;
  let fixture: ComponentFixture<TeamManagementHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamManagementHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamManagementHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
