import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AppFunctions } from 'src/app/app-functions';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  public serverIp = this.systemConfig.serverIp;
  public loginProgress = "false";
  public showForgetPassword = "false";
  public userAccessCode = this.systemConfig.adminAccessCode;
  public serverResponse = "";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  seconds: number = this.systemConfig.errorResetTime;
  interval;

  constructor(private postToServerObject: HttpRequestResponseService,private systemConfig: SystemConfig,
    private router: Router,private golbalFuncs: AppFunctions) { }

  
  ngOnInit(): void {
    if( this.golbalFuncs.isSessionSet() ){
      console.log("user already logged in, redirecting...");
      let navigationExtras: NavigationExtras = {
        queryParams: {
            redirectTo: localStorage.getItem('UserAccess')
        }
      }
        this.router.navigate([this.systemConfig.redirectURL], navigationExtras);
    }else{
      this.validateServer(this.serverIp);
    }

  }


  onSubmit(event,username,password){

    if((username != "") && (password != "")){
      this.userLogin(username,password);
      this.loginProgress = "true";
    }else{
      this.serverStatus_Error_Class = this.systemConfig.errorClass;
      this.serverStatus_Error_Message = "please enter EmployId and Password to login.";
      this.startTimer();
    }
    
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
    this.seconds = this.systemConfig.errorResetTime;
  }

  public closealertbtn(){
    this.serverStatus_Error_Class = "";
    this.serverStatus_Error_Message = "";
  }

  public userLogin(username,password){

    this.postToServerObject.login(username,password).subscribe((res: HttpResponse<any>)=> {
      var loginStatus: Boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      this.loginProgress = "false";
      loginStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(!loginStatus){

        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        this.startTimer();
        if(this.serverStatus_Error_Message == "UserName or Password is wrong, please try again."){
          // open forget password page.
          this.showForgetPassword = "true";
        }
        
      }else{
        if(parseInt(this.serverResponse["EvertzManagementApp"]["ParameterList"]["UserAccess"]) == parseInt(this.userAccessCode)){

          //set session
          let ename = this.serverResponse["EvertzManagementApp"]["ParameterList"]["FirstName"] + " " + this.serverResponse["EvertzManagementApp"]["ParameterList"]["MiddleName"] + " " + this.serverResponse["EvertzManagementApp"]["ParameterList"]["LastName"];
          let eid = this.serverResponse["EvertzManagementApp"]["ParameterList"]["EmployeeId"];
          let isNewUser = this.serverResponse["EvertzManagementApp"]["ParameterList"]["NewUser"];

          if((localStorage.getItem(username+".ProfilePic") == null) || (localStorage.getItem(username+".ProfilePic") == this.systemConfig.profileLoadingPic)){
            console.log("new user");
            this.golbalFuncs.enableSession(this.userAccessCode,username,ename,eid,true,isNewUser);
          }else{
            this.golbalFuncs.enableSession(this.userAccessCode,username,ename,eid,false,isNewUser);
          }

          if(isNewUser){
            this.router.navigate([this.systemConfig.newEmployeeDetails]);
          }else{
            this.router.navigate([this.systemConfig.adminHome]);
          }
          
        }else{
          this.serverStatus_Error_Class = this.systemConfig.errorClass;
          this.serverStatus_Error_Message = "You dont have access rights for using this page.";
          this.startTimer();
        }
        
      }

    },(err: HttpErrorResponse) => {  
      this.loginProgress = "false";
      console.log("login timed out");
        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = "Connection timed out, please try again latter."
       // this.startTimer();
    });    
  }
  
  public validateServer(serverIp){
    this.postToServerObject.validateServer(serverIp).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      this.loginProgress = "false";
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(!serverStatus){

        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        console.log(this.serverStatus_Error_Message);
        this.serverStatus_Error_Message = "Server failed to connect, please try again latter.";
        this.startTimer();
      }

    },(err: HttpErrorResponse) => {  
      this.loginProgress = "false";
      this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = "Connection timed out, please try again latter.";
        //this.startTimer();
    }); 
  }

}
