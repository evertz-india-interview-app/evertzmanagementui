import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import * as bootstrap from 'bootstrap';
import * as $ from 'jquery';
import {MatDialogModule} from "@angular/material/dialog";
import { EmployeeLoginComponent } from './employee/employee-login/employee-login.component';
import { SystemConfig } from './wsCall/system-config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeeHomePageComponent } from './employee/employee-home-page/employee-home-page.component';
import { SessionRouterComponent } from './global/session-router/session-router.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home.component';
import { FinanceHomeComponent } from './finance/finance-home/finance-home.component';
import { FinanceLoginComponent } from './finance/finance-login/finance-login.component';
import { HrLoginComponent } from './hr/hr-login/hr-login.component';
import { HrHomeComponent } from './hr/hr-home/hr-home.component';
import { TeamManagementHomeComponent } from './team_management/team-management-home/team-management-home.component';
import { TeamManagementLoginComponent } from './team_management/team-management-login/team-management-login.component';
import { ForgotPasswordComponent } from './global/forgot-password/forgot-password.component';
import { AdminToolsComponent } from './admin/admin-tools/admin-tools.component';
import {MatCardModule} from '@angular/material/card';
import { SessionDialogueComponent } from './global/session-dialogue/session-dialogue.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NoRightClickDirective } from './global/no-right-click.directive';
import { LoadingBarModule } from '../../node_modules/ngx-loading-bar';
import { NewEmployeeDetailsComponent } from './global/new-employee-details/new-employee-details.component';
import { EmployeeLeavesComponent } from './employee/employee-leaves/employee-leaves.component';
import { AppFunctions } from './app-functions';
import { MatDatepickerModule, } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule, MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';



@NgModule({
  declarations: [
    AppComponent,
    EmployeeLoginComponent,
    EmployeeHomePageComponent,
    SessionRouterComponent,
    AdminLoginComponent,
    AdminHomeComponent,
    FinanceHomeComponent,
    FinanceLoginComponent,
    HrLoginComponent,
    HrHomeComponent,
    TeamManagementHomeComponent,
    TeamManagementLoginComponent,
    ForgotPasswordComponent,
    AdminToolsComponent,
    SessionDialogueComponent,
    NoRightClickDirective,
    NewEmployeeDetailsComponent,
    EmployeeLeavesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatDialogModule,
    LoadingBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule
  ],
  providers: [
    SystemConfig,
    AppFunctions,{
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: { color: 'warn' }} //warn,primary,accent
    ],
  bootstrap: [AppComponent],
  entryComponents: [SessionDialogueComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
