import { Component, OnInit, HostListener } from '@angular/core';
import { SystemConfig } from 'src/app/wsCall/system-config';


@Component({
  selector: 'app-employee-leaves',
  templateUrl: './employee-leaves.component.html',
  styleUrls: ['./employee-leaves.component.css']
})
export class EmployeeLeavesComponent implements OnInit {

  constructor(private systemConfig: SystemConfig) { }

  ngOnInit(): void {

    this.startSessionTimer();

  }

  currDiv: string = 'none';
  @HostListener('click') onSelect(divVal:string){
    if(this.currDiv=='none'){
      this.currDiv = divVal;
    }
    else{
      this.currDiv='none';
    }
  }

  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  public sessionResetTime = this.systemConfig.sessionTime;
  seconds: number = this.systemConfig.sessionTime;
  interval;

  startSessionTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        console.log("Session ended");
      }
    },1000)
  }

  displayFunctionCode: string = '2';
  @HostListener('click') onApply(divVal:string,){
    if(this.displayFunctionCode=='2'){
      this.displayFunctionCode = divVal;
    }
    else if(this.displayFunctionCode=='1'){
      this.displayFunctionCode=divVal;
    }
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    this.seconds = this.sessionResetTime;
  }

  @HostListener('click') onClick() {
    this.seconds = this.sessionResetTime;
    
  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {
    this.seconds = this.sessionResetTime;
  }

  checkDropDown(){

    if(this.currDiv=='dropdown-content'){
      this.currDiv='none';
    }
  }

}
