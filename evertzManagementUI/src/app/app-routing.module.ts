import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeLoginComponent } from './employee/employee-login/employee-login.component';
import { EmployeeHomePageComponent } from './employee/employee-home-page/employee-home-page.component';
import { SessionRouterComponent } from './global/session-router/session-router.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home.component';
import { FinanceHomeComponent } from './finance/finance-home/finance-home.component';
import { FinanceLoginComponent } from './finance/finance-login/finance-login.component';
import { HrLoginComponent } from './hr/hr-login/hr-login.component';
import { HrHomeComponent } from './hr/hr-home/hr-home.component';
import { TeamManagementHomeComponent } from './team_management/team-management-home/team-management-home.component';
import { TeamManagementLoginComponent } from './team_management/team-management-login/team-management-login.component';
import { ForgotPasswordComponent } from './global/forgot-password/forgot-password.component';
import { AdminToolsComponent } from './admin/admin-tools/admin-tools.component';
import { NewEmployeeDetailsComponent } from './global/new-employee-details/new-employee-details.component';
import { EmployeeLeavesComponent } from './employee/employee-leaves/employee-leaves.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: EmployeeLoginComponent },
  { path: 'ehome', component: EmployeeHomePageComponent },
  { path: 'eleave', component: EmployeeLeavesComponent },

  
  { path: 'admin.login', component: AdminLoginComponent },
  { path: 'admin.home', component: AdminHomeComponent },
  { path: 'admin.tools', component: AdminToolsComponent },

  { path: 'finance.login', component: FinanceLoginComponent },
  { path: 'finance.home', component: FinanceHomeComponent },
  
  { path: 'hr.login', component: HrLoginComponent },
  { path: 'hr.home', component: HrHomeComponent },
  
  { path: 'redirect', component: SessionRouterComponent },
  { path: 'resetpwd', component: ForgotPasswordComponent },

  { path: 'edetails', component: NewEmployeeDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
