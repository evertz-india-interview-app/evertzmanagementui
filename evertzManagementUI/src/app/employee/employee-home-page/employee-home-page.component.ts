import { Component, OnInit, HostListener } from '@angular/core';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { Router } from '@angular/router';
import { AppFunctions } from 'src/app/app-functions';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';

@Component({
  selector: 'app-employee-home-page',
  templateUrl: './employee-home-page.component.html',
  styleUrls: ['./employee-home-page.component.css']
})
export class EmployeeHomePageComponent implements OnInit {

  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  public employeeName: any;
  public employeeCode: any;
  public employeeProfilePic: any;
  public serverResponse: any;
  public holidayList = [];
  public sessionResetTime = this.systemConfig.sessionTime;
  seconds: number = this.systemConfig.sessionTime;
  resetseconds: number = this.systemConfig.errorResetTime;
  interval;

  constructor(private systemConfig: SystemConfig,private postToServerObject: HttpRequestResponseService
    ,private redirect: Router,private golbalFuncs: AppFunctions) { }

  ngOnInit(): void {

    if(this.golbalFuncs.isSessionSet() && parseInt(localStorage.getItem("UserAccess"))==1){

      this.golbalFuncs.isSessionTimedOut = false;
      this.golbalFuncs.startSessionTimer();
      this.employeeCode = localStorage.getItem("EmployeeCode");
      this.employeeName = localStorage.getItem("EmployeeName");
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
      if(this.employeeProfilePic == null){
        this.employeeProfilePic = this.systemConfig.profileLoadingPic;
      }

    }else{
      //invalid session
      this.golbalFuncs.resetSession();
      this.redirect.navigate([this.systemConfig.employeeLogin]);
    }

    if(this.golbalFuncs.isNewUser()){
      this.redirect.navigate([this.systemConfig.newEmployeeDetails]);
    }

    this.holidayList=[
      {"Date":"2020-05-01","Name":"May Day"},
      {"Date":"2020-05-25","Name":"Ramzan"},
      {"Date":"2020-07-31","Name":"Eid-alah"},
      {"Date":"2020-07-31","Name":"Mahatma Gandhi Jayanti all day holiday"}
    ];

  }

  currDiv: string = 'none';
  @HostListener('click') onSelect(divVal:string){
    if(this.currDiv=='none'){
      this.currDiv = divVal;
    }
    else{
      this.currDiv='none';
    }
  }


  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  @HostListener('click') onClick() {
    this.golbalFuncs.sessionseconds = this.sessionResetTime;    
  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {

    if(this.employeeProfilePic != localStorage.getItem(this.golbalFuncs.userProfilePic)){
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
    }
    this.golbalFuncs.sessionseconds = this.sessionResetTime;
  }

  checkDropDown(){

    if(this.currDiv=='dropdown-content'){
      this.currDiv='none';
    }
  }

  convertToDateAlpha(date){
    return this.golbalFuncs.formatDate(date);
  }

  closealertbtn(){
    this.serverStatus_Error_Class = "";
    this.serverStatus_Error_Message = "";
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.resetseconds > 0) {
        this.resetseconds--;
      } else {
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
    this.resetseconds = this.systemConfig.errorResetTime;
  }


  userLogout(){
    this.golbalFuncs.userLogout();
  }

  public getProfilePic(){
    this.postToServerObject.getProfilePic(this.employeeCode).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(serverStatus){

        let profilePic = this.serverResponse["EvertzManagementApp"]["ParameterList"]["ProfilePicBase64"];
        localStorage.setItem("ProfilePic",profilePic);

      }else{
        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message =this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        //this.startTimer();
      }

    },(err: HttpErrorResponse) => {  
      this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = "Connection timed out, while loading profile pic, please try again latter.";
        //this.startTimer();
    }); 
  }
 
}
