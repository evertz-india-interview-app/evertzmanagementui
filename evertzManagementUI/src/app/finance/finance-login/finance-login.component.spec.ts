import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceLoginComponent } from './finance-login.component';

describe('FinanceLoginComponent', () => {
  let component: FinanceLoginComponent;
  let fixture: ComponentFixture<FinanceLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
