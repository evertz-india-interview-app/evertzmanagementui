import { SystemConfig } from './wsCall/system-config';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SessionDialogueComponent } from './global/session-dialogue/session-dialogue.component';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpRequestResponseService } from './http-request-response.service';

export class AppFunctions {
    
    public userProfilePic: any = localStorage.getItem("EmployeeCode") + ".ProfilePic";
    public serverResponse: any;
    public sessionResetTime = this.systemConfig.sessionTime;
    sessionseconds: number =  this.systemConfig.sessionTime;
    public isSessionTimedOut = false;
    interval;
    
    constructor(private systemConfig: SystemConfig,private dialog: MatDialog,
        private postToServerObject: HttpRequestResponseService,private redirect: Router) {}

    //To Set Session Across all components
    enableSession(useracces,ecode,ename,eid,profileNeedsChange,isNewUser){

        this.userProfilePic = ecode + ".ProfilePic";
        console.log(this.userProfilePic);
        localStorage.setItem("UserAccess",useracces);
        localStorage.setItem("EmployeeCode",ecode);
        localStorage.setItem("EmployeeName",ename);
        localStorage.setItem("EmployeeId",eid);
        localStorage.setItem("IsNewUser",isNewUser);

        if(profileNeedsChange){
            console.log("calling get profile pic");
            this.getProfilePic();
        }else{
            //console.log("loading pic from session");
            //localStorage.setItem(this.userProfilePic,this.systemConfig.profileLoadingPic);
        }
        
    }

    //To Reset the Session across all components
    resetSession(){
        this.isSessionTimedOut = true;
        localStorage.removeItem("UserAccess");
        localStorage.removeItem("EmployeeCode");
        localStorage.removeItem("EmployeeName");
        localStorage.removeItem("EmployeeId");
        localStorage.removeItem("IsNewUser");
        //localStorage.removeItem(this.userProfilePic);
    }


    isNewUser(){
        if(localStorage.getItem("IsNewUser") == "true"){
            return true;
        }else{
            return false;
        }
    }

    isSessionSet(){
        if(localStorage.getItem("UserAccess")!= null && 
        localStorage.getItem("EmployeeCode")!= null &&
        localStorage.getItem("EmployeeName")!= null &&
        localStorage.getItem("EmployeeId")!= null &&
        localStorage.getItem("IsNewUser")!= null
        ){
            console.log("session set, returning true");
            return true;
        }else{
            console.log("session not set, returning false");
            return false;
        }
    }

    startSessionTimer() {
        this.interval = setInterval(() => {
          if(this.sessionseconds > 0) {
            if(this.isSessionTimedOut == false){
              this.sessionseconds--;
            }
            
          } else {
            if(this.isSessionTimedOut == false){
                this.isSessionTimedOut = true;
                this.openDialog();
            }
            
          }
        },1000)
    }


    openDialog() {

        const dialogConfig = new MatDialogConfig();
    
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        
        const dialogRef = this.dialog.open(SessionDialogueComponent,dialogConfig);
        
        dialogRef.afterClosed().subscribe(result => {
          // release session and navegate to next page
          this.userLogout();
        });
    
    }

    userLogout(){

        let employeeCode: any = localStorage.getItem("EmployeeCode");
        this.postToServerObject.logout(employeeCode).subscribe((res: HttpResponse<any>)=> {
          this.isSessionTimedOut = true;
          this.redirect.navigate([this.redirectURL()]);
    
        },(err: HttpErrorResponse) => {  
            this.isSessionTimedOut = true;
            this.redirect.navigate([this.redirectURL()]);
        });    
    }

    redirectURL(){

        if(parseInt(localStorage.getItem("UserAccess")) == 1){
            this.resetSession();
            return this.systemConfig.employeeLogin;
        }else if(parseInt(localStorage.getItem("UserAccess")) == 2){

        }else if(parseInt(localStorage.getItem("UserAccess")) == 3){
            
        }else if(parseInt(localStorage.getItem("UserAccess")) == 4){
            
        }else if(parseInt(localStorage.getItem("UserAccess")) == 5){
            this.resetSession();
            return this.systemConfig.adminLogin
        }else{
            this.resetSession();
            return this.systemConfig.employeeLogin;
        }

    }
    
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    public getProfilePic(){

        let employeeCode: any = localStorage.getItem("EmployeeCode");
        this.postToServerObject.getProfilePic(employeeCode).subscribe((res: HttpResponse<any>)=> {
          var serverStatus: boolean;
          this.serverResponse = JSON.parse(JSON.stringify(res.body));
          serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
          console.log(this.serverResponse);
          if(serverStatus){
    
            console.log("setting profile pic from db");
            let profilePic = this.serverResponse["EvertzManagementApp"]["ParameterList"]["ProfilePicBase64"];
            localStorage.setItem(this.userProfilePic,profilePic);
    
          }else{
            console.log(this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"]);
          }
          

        },(err: HttpErrorResponse) => {  
            
        }); 
    }

}