import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamManagementLoginComponent } from './team-management-login.component';

describe('TeamManagementLoginComponent', () => {
  let component: TeamManagementLoginComponent;
  let fixture: ComponentFixture<TeamManagementLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamManagementLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamManagementLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
