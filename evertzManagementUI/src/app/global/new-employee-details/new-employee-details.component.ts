import { Component, OnInit, HostListener, OnChanges } from '@angular/core';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { AppFunctions } from 'src/app/app-functions';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';
import { Router } from '@angular/router';
import { LoadingBarService } from '../../../../node_modules/ngx-loading-bar';


@Component({
  selector: 'app-new-employee-details',
  templateUrl: './new-employee-details.component.html',
  styleUrls: ['./new-employee-details.component.css']
})
export class NewEmployeeDetailsComponent {

  seconds: number = this.systemConfig.sessionTime;
  resetseconds: number = this.systemConfig.errorResetTime;
  interval;
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  public employeeName: any;
  public employeeCode: any;
  public employeeProfilePic: any;
  public selectedProfilePic: File;
  public serverResponse: any;
  public showProgressBar = false;
  public isGrade: 'yes' | 'no' = 'no';
  public isExperienced: 'yes' | 'no' = 'no';
  public isVisaHolder: 'yes' | 'no' = 'no';
  public isPassportHolder: 'yes' | 'no' = 'no';
  public grade = "";
  public percentage = "";
  public prevCompany: any = "";
  public prevCompanyDesignation: any = "";
  public workdFrom: any = "";
  public workedTill: any = "";
  public vcountry: any = "";
  public vnumber: any = "";
  public vexpirey: any = "";
  public educationType: any;
  public educationBranch: any;
  public gradeList: any;
  public docType: any;
  public passportCountry: any;
  public bankNameList: any;
  public selectedbank = "";
  public bankBranchList: any;
  public selectedbranch: any;
  public selectedbranchId: any;
  public passportName: any = "";
  public passportNumber: any = "";
  public passportIssueDay: any = "";
  public passportExpDay: any = "";



  constructor(private systemConfig: SystemConfig,private postToServerObject: HttpRequestResponseService
    ,private redirect: Router,private golbalFuncs: AppFunctions,private loadingBar: LoadingBarService) { }


  ngOnInit(): void {

    
    if(this.golbalFuncs.isSessionSet()){

      this.employeeCode = localStorage.getItem("EmployeeCode");
      this.employeeName = localStorage.getItem("EmployeeName");
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
      if(this.employeeProfilePic == null){
        this.employeeProfilePic = this.systemConfig.profileLoadingPic;
      }

      this.getUIDropDownValues();

      this.selectedbranch = [{
        "BankBranch": "",
        "IFSCCode": "",
        "Id": 0,
        "SwiftCode": ""
      }];

    }else{
      //invalid session
      this.golbalFuncs.resetSession();
      this.redirect.navigate([this.systemConfig.employeeLogin]);
    }

    if(!this.golbalFuncs.isNewUser()){
      this.redirect.navigate([this.systemConfig.employeehome]);
    }

  }
  

  currDiv: string = 'none';
  @HostListener('click') onSelect(divVal:string){
    if(this.currDiv=='none'){
      this.currDiv = divVal;
    }
    else{
      this.currDiv='none';
    }

  }
  
  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    
  }
  
  @HostListener('click') onClick() {

  }

  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {
    
    if(this.employeeProfilePic != localStorage.getItem(this.golbalFuncs.userProfilePic)){
      this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
    }

  }

  checkDropDown(){

    if(this.currDiv=='dropdown-content'){
      this.currDiv='none';
    }
  }

  closealertbtn(){
    this.serverStatus_Error_Class = "";
    this.serverStatus_Error_Message = "";
  }
  
  startTimer() {
    this.interval = setInterval(() => {
      if(this.resetseconds > 0) {
        this.resetseconds--;
      } else {
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
    this.resetseconds = this.systemConfig.errorResetTime;
  }


  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

  userLogout(){

    this.postToServerObject.logout(this.employeeCode).subscribe((res: HttpResponse<any>)=> {
      this.golbalFuncs.resetSession();
      this.redirect.navigate([this.systemConfig.employeeLogin]);

    },(err: HttpErrorResponse) => {  
        this.golbalFuncs.resetSession();
        this.redirect.navigate([this.systemConfig.employeeLogin]);
    });    
  }

  changeProfilePic(event){
    this.selectedProfilePic = event.target.files[0];
    console.log(this.selectedProfilePic);
    var reader = new FileReader();
    reader.readAsDataURL(this.selectedProfilePic);
    reader.onloadend = e => {
            
      var profilePicBase64 = reader.result;
      this.updateProfilePic(profilePicBase64);

     }

  }

  public updateProfilePic(profileBase64){
    this.postToServerObject.updateProfilePic(this.employeeCode,profileBase64).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(serverStatus){

        
        localStorage.setItem(this.golbalFuncs.userProfilePic,profileBase64);
        this.employeeProfilePic = localStorage.getItem(this.golbalFuncs.userProfilePic);
        
      }else{
        this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message =this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        this.startTimer();
      }


    },(err: HttpErrorResponse) => {  
      this.serverStatus_Error_Class = this.systemConfig.errorClass;
        this.serverStatus_Error_Message = "Connection timed out, please try again latter.";
        this.startTimer();
    }); 
  }


  getBranchDetails(){

    for(let i=0;i<this.bankBranchList.length;i++){
      if(this.bankBranchList[i]["Id"]==this.selectedbranchId){
        this.selectedbranch = this.bankBranchList[i];
      }
    }

  }

  getBankBranchList(){

    console.log(this.selectedbank);
    let bankId: any = this.selectedbank.toString();
    this.postToServerObject.getBankBranchList(bankId,this.employeeCode).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: Boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(!serverStatus){
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        console.log(this.serverStatus_Error_Message);
        this.startTimer();
      }else{

        this.bankBranchList = this.serverResponse["EvertzManagementApp"]["ParameterList"]["BankBranchList"];

      }

    },(err: HttpErrorResponse) => { 
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = "Failed to Connect to Server, please try again latter.";
        this.startTimer();
    });
  }


  getUIDropDownValues(){
    var options: any;
    options= ["EducationType","EducationBranch","DocumentType","Country","BankName","Grade"];
    this.postToServerObject.getUIDropdown(options,this.employeeCode).subscribe((res: HttpResponse<any>)=> {
      var serverStatus: Boolean;
      this.serverResponse = JSON.parse(JSON.stringify(res.body));
      serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
      console.log(this.serverResponse);
      if(!serverStatus){
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
        console.log(this.serverStatus_Error_Message);
        this.startTimer();
      }else{

        this.educationType = this.serverResponse["EvertzManagementApp"]["ParameterList"]["EducationTypeList"];
        this.educationBranch = this.serverResponse["EvertzManagementApp"]["ParameterList"]["EducationBranchList"];
        this.docType = this.serverResponse["EvertzManagementApp"]["ParameterList"]["DocumentTypeList"];
        this.passportCountry = this.serverResponse["EvertzManagementApp"]["ParameterList"]["CountryList"];
        this.bankNameList = this.serverResponse["EvertzManagementApp"]["ParameterList"]["BankNameList"];
        this.gradeList = this.serverResponse["EvertzManagementApp"]["ParameterList"]["GradeList"];

      }

    },(err: HttpErrorResponse) => { 
        this.serverStatus_Error_Class = this.systemConfig.errorClass;;
        this.serverStatus_Error_Message = "Failed to Connect to Server, please try again latter.";
        this.startTimer();
    }); 
  }


  addEmployeeDetailsToDB(dob,email,mobile,
    edutype,edubranch,
    doc1,doc1val,doc2,doc2val,
    ebank,accno,ebankbranch,accname){
    this.showProgressBar = true;
    /*console.log(dob);
    console.log(email);
    console.log(mobile);
    console.log(edutype);
    console.log(edubranch);
    console.log(this.grade);
    console.log(this.percentage);

    console.log(doc1);
    console.log(doc2);
    console.log(doc1val);
    console.log(doc2val);

    console.log(this.vcountry);
    console.log(this.vnumber);
    console.log(this.vexpirey);

    console.log(ebank);
    console.log(accno);
    console.log(ebankbranch);
    console.log(accname); */
    
    let errorMsg = "";

    if(dob == ""){
      errorMsg += " Date of Birth Can't be empty,";
    }else{
      dob= this.golbalFuncs.formatDate(dob);
    }
    if(email == ""){
      errorMsg += " Email Can't be empty,";
    }
    /*if(email.match("^([a-z][A-Z][1-9])")){

    }else{
      errorMsg += " Please Provide a Valid Email ID,";
    }*/
    if(mobile == ""){
      errorMsg += " Mobile Number Can't be empty,";
    }if(mobile.length > 13){
      errorMsg += " Mobile Number is having more length than allowed,";
    }if(edutype == ""){
      errorMsg += " Education Type Can't be empty,";
    }
    if(edubranch == ""){
      errorMsg += " Education Branch Can't be empty,";
    }

    if(this.isGrade == "yes"){
      if(this.grade == ""){
        errorMsg += " Grade Can't be empty,";
      }
    }else{
      if(this.percentage == ""){
        errorMsg += " Percentage Can't be empty,";
      }else if(this.percentage.match("^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$")){
        //console.log("valid %");
      }else{
        errorMsg += " Please provide percentage in valid format (e.g 92.3 or 92.33),";
      }
    }

    if(doc1 == doc2){
      errorMsg += " Document1 Document2 Can't be Same,";
    }
    if(doc1val == ""){
      errorMsg += " Document1 Can't be empty,";
    }
    if( doc2val == ""){
      errorMsg += " Document2 Can't be empty,";
    }

    if(this.isExperienced =="yes"){
      if(this.prevCompany == ""){
        errorMsg += " Previous Company Name Can't be empty,";
      }
      if(this.prevCompanyDesignation == ""){
        errorMsg += " Previous Company Designation Can't be empty,";
      }if(this.workdFrom == ""){
        errorMsg += " Worked From Date Can't be empty,";
      }else{
        this.workdFrom= this.golbalFuncs.formatDate(this.workdFrom);
      }
      if(this.workedTill == ""){
        errorMsg += " Worked Till Date Can't be empty,";
      }else{
        this.workedTill= this.golbalFuncs.formatDate(this.workedTill);
      }
    }


    if(this.isPassportHolder == "yes"){
      var passIssueDate = new Date(this.passportIssueDay);
      var passExpDate = new Date(this.passportExpDay);
      if(passExpDate < passIssueDate){
        errorMsg += " Passport Isseu/Expirey Date is wrong,";
      }
      if(this.passportName == ""){
        errorMsg += " Name On Passport Date Can't be empty,";
      }if(this.passportNumber == ""){
        errorMsg += " Passport Number Can't be empty,";
      }if(this.passportIssueDay == ""){
        errorMsg += " Passport Issue Date Can't be empty,";
      }else{
        this.passportIssueDay= this.golbalFuncs.formatDate(this.passportIssueDay);
      }
      if(this.passportExpDay == ""){
        errorMsg += " Passport Expirey Date Can't be empty,";
      }else{
        this.passportExpDay= this.golbalFuncs.formatDate(this.passportExpDay);
      }

      if(this.isVisaHolder == "yes"){
        if(this.vcountry == ""){
          errorMsg += " Visa Country Can't be empty,";
        }if(this.vnumber == ""){
          errorMsg += " Visa Number Can't be empty,";
        }if(this.vexpirey == ""){
          errorMsg += " Visa Expirey Date Can't be empty,";
        }else{
          this.vexpirey= this.golbalFuncs.formatDate(this.vexpirey);
        }
      }
      
    }


    if(ebank ==""){
      errorMsg += " Bank Name Can't be empty,";
    }if(accno ==""){
      errorMsg += " Bank Account Number Can't be empty,";
    }if(ebankbranch ==""){
      errorMsg += " Bank Branch Can't be empty,";
    }if(accname ==""){
      errorMsg += " Bank Account Holder Name Can't be empty,";
    }


    if(errorMsg !=""){

      this.serverStatus_Error_Class = this.systemConfig.errorClass;
      this.serverStatus_Error_Message = errorMsg + " Please Provide valid Data to Proceed...";
      this.showProgressBar = false;
      this.startTimer();
      this.gotoTop();

    }else{
      console.log("ready for WS-call");

      let isGradeset: any = this.returBoolean(this.isGrade);
      let isExperiencedset: any = this.returBoolean(this.isExperienced);
      let isPassportHolderset: any = this.returBoolean(this.isPassportHolder);
      let isVisaHolderset: any = this.returBoolean(this.isVisaHolder);
      if(!isPassportHolderset){
        isVisaHolderset = false;
      }
      let emploeeId: any = localStorage.getItem("EmployeeId").toString();
      let scoreValue: any = "";
      let selectedBankBranchId: any = String(this.selectedbranch["Id"]);
      if(isGradeset){
        scoreValue = this.grade;
      }else{
        scoreValue = this.percentage;
      }
      
      console.log("sending WS-call");
      this.postToServerObject.updateEmployeeFirstTimeDetails(this.employeeCode,emploeeId,dob,
      email,mobile,isGradeset,edutype,edubranch,scoreValue,doc1,doc1val,doc2,doc2val,isExperiencedset,
      this.prevCompany,this.prevCompanyDesignation,this.workdFrom,this.workedTill,isVisaHolderset,isPassportHolderset,
      this.passportNumber,this.passportNumber,this.passportIssueDay,this.passportExpDay,this.vcountry,this.vnumber,this.vexpirey,
      accno,accname,selectedBankBranchId).subscribe((res: HttpResponse<any>)=> {
        var serverStatus: Boolean;
        this.serverResponse = JSON.parse(JSON.stringify(res.body));
        serverStatus = this.serverResponse["EvertzManagementApp"]["Success"];
        console.log(this.serverResponse);
        this.showProgressBar = false;
        if(!serverStatus){
  
          this.serverStatus_Error_Class = "alert alert-danger";
          this.serverStatus_Error_Message = this.serverResponse["EvertzManagementApp"]["ParameterList"]["Reason"];
          console.log(this.serverStatus_Error_Message);
          this.gotoTop();
          this.startTimer();

        }else{

          console.log("emploee firs time details updated");
          //set session and route to employee home page
          localStorage.setItem("isNewUser","false");
          localStorage.setItem("UserAccess","1");
          this.redirect.navigate([this.systemConfig.employeehome]);


        }
  
      },(err: HttpErrorResponse) => {  
        this.serverStatus_Error_Class = "alert alert-danger";
          this.serverStatus_Error_Message = "Connection timed out, please try again latter.";
          this.showProgressBar = false;
          this.gotoTop();
          this.startTimer();
      }); 


    }

  }

  returBoolean(val){
    if(val == "yes"){
      return true;
    }else if(val== "no"){
      return false;
    }

  }

}
