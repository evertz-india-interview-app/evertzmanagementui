import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-session-dialogue',
  templateUrl: './session-dialogue.component.html',
  styleUrls: ['./session-dialogue.component.css']
})
export class SessionDialogueComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<SessionDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) data) { }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close("UserAgreed");
  }
}
