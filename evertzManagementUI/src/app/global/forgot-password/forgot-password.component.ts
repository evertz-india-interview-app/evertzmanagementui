import { Component, OnInit } from '@angular/core';
import { HttpRequestResponseService } from 'src/app/http-request-response.service';
import { SystemConfig } from 'src/app/wsCall/system-config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private postToServerObject: HttpRequestResponseService,private systemConfig: SystemConfig,private router: Router) { }

  ngOnInit(): void {
  }

  public requestOnProgress = "false";
  public serverStatus_Error_Class = "";
  public serverStatus_Error_Message = "";
  seconds: number = this.systemConfig.errorResetTime;
  interval;

  onSubmit(event,username){

    if((username != "")){
      
      this.requestOnProgress = "true";
    }else{
      this.serverStatus_Error_Class = "alert alert-danger";
      this.serverStatus_Error_Message = "please enter EmployId to Send Reset Request.";
      this.startTimer();
    }
    
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.seconds > 0) {
        this.seconds--;
      } else {
        this.serverStatus_Error_Class = "";
        this.serverStatus_Error_Message = "";
      }
    },1000)
    this.seconds = this.systemConfig.errorResetTime;
  }

}

